FROM romiafrizal/php7.4-base-image
WORKDIR /var/www/html
COPY . .
RUN composer install 
EXPOSE 80
CMD ["/usr/bin/supervisord"]